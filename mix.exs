defmodule Tweetest.Mixfile do
  use Mix.Project

  def project do
    [app: :tweetest,
     version: "0.1.0",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [extra_applications: [:logger, :extwitter],
     mod: {Tweetest.Application, []}]
  end

  defp deps do
    [
      {:oauther, "~> 1.1"},
      {:extwitter, "~> 0.9.0"},
    ]
  end
end
