defmodule Tweetest do
  @moduledoc false

  def stream_sample() do
    stream = ExTwitter.stream_sample(receive_messages: true)
    for message <- stream do
      case message do
        tweet = %ExTwitter.Model.Tweet{} ->
          IO.puts "tweet = #{tweet.text}"
    
        deleted_tweet = %ExTwitter.Model.DeletedTweet{} ->
          IO.puts "deleted tweet = #{deleted_tweet.status[:id]}"
    
        limit = %ExTwitter.Model.Limit{} ->
          IO.puts "limit = #{limit.track}"
    
        stall_warning = %ExTwitter.Model.StallWarning{} ->
          IO.puts "stall warning = #{stall_warning.code}"
    
        _ ->
          IO.inspect message
      end
    end
  end

  def stream_filter(what, timeout) do
    pid = spawn(fn ->
      stream = ExTwitter.stream_filter(track: what)
      for tweet <- stream do
        IO.puts tweet.text
      end
    end)
    
    :timer.sleep(timeout * 1000)
    ExTwitter.stream_control(pid, :stop)
  end
end
